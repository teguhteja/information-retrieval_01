/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.db;

import com.ttm.simpleir.calc.MyIdf;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author palgunadi
 */
public class Model {
    SQLiteJDBC sQLiteJDBC;
    
    public Model() throws Exception{
        sQLiteJDBC = new SQLiteJDBC();
        sQLiteJDBC.open();
    }

    public Object[][] selectWeightTerm(String term) throws SQLException {
        ArrayList<MyWeightDoc> arrLMyWeightTerm = new ArrayList<>();
        Object[][] retVal = null;
        String sql="select a.id_doc, a.name_doc as doc, b.name_term, b.tf, c.idf, (b.tf*c.idf) as weight \n" +
                    "from t_doc a left join t_tf b on a.id_doc = b.id_doc \n" +
                    "	left join t_idf c on b.name_term = c.name_term\n" +
                    "where b.name_term = '"+term+"'\n"+	
                    "order by weight desc";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        //preparedStatement.setString(0, term);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            MyWeightDoc mwd = new MyWeightDoc();
            mwd.term = rs.getString("doc");
            mwd.dWeight = rs.getDouble("weight");
            arrLMyWeightTerm.add(mwd);
        }
        retVal = new Object[arrLMyWeightTerm.size()][2];
        for(int i=0;i<retVal.length;i++){
            MyWeightDoc mwd = arrLMyWeightTerm.get(i);
            retVal[i][0] = mwd.term;
            retVal[i][1] = mwd.dWeight; 
        }
        return retVal;
    }

    public Double selectIdf(String term) throws SQLException {
        Double retVal = Double.MAX_VALUE;
        String sql="SELECT idf FROM t_idf where name_term = ?";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, term);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            retVal = rs.getDouble("idf");
        }
        return retVal;
    }

    public Double selectWeightBasedIdTerm(int id, String term) throws SQLException {
        Double retVal = Double.MAX_VALUE;
        String sql = "select a.id_doc, a.name_term, a.tf, b.idf, (a.tf*b.idf) as weight \n" +
            " from t_tf a left join t_idf b on a.name_term = b.name_term\n" +
            " where a.name_term = '"+term+"' and a.id_doc = ? ";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, id);
        //preparedStatement.setString(0, term);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            retVal = rs.getDouble("weight");
        }
        return retVal;
    }
    
    class MyWeightDoc{
        String term; // bisa dipakai buat doc juga
        Double dWeight;
    }

    public void insert2t_doc(String filename, String content) throws SQLException {
        String sql="INSERT INTO t_doc(name_doc,content_doc) VALUES(?,?)";
        
        Connection conn = sQLiteJDBC.getConnection();
        PreparedStatement pmst = conn.prepareStatement(sql);
        pmst.setString(1, filename);
        pmst.setString(2, content);
        pmst.execute();
    }

    public int selectIdinDoc(String filename) throws SQLException {
        int retVal = 0;
        String sql="SELECT id_doc FROM t_doc";
        Connection conn =  sQLiteJDBC.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        while(rs.next()){
            retVal = rs.getInt("id_doc");
        }
        return retVal;
    }

    public void insert2t_term(int id_doc, String term, float freq) throws SQLException {
        String sql="INSERT INTO t_tf(id_doc,name_term, tf) VALUES(?,?,?)";
        
        Connection conn = sQLiteJDBC.getConnection();
        PreparedStatement pmst = conn.prepareStatement(sql);
        pmst.setInt(1, id_doc);
        pmst.setString(2, term);
        pmst.setFloat(3, freq);
        pmst.execute();
    }

    public int selectCountDoc() throws SQLException {
        int retVal = 0;
        String sql="SELECT count(id_doc) as COUNT FROM t_doc";
        Connection conn =  sQLiteJDBC.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        while(rs.next()){
            retVal = rs.getInt("COUNT");
        }
        return retVal;    }

    public ArrayList<MyIdf> selectDistTerm() throws SQLException {
        ArrayList<MyIdf> retVal = new ArrayList<>();
        String sql="select name_term, count(id_doc) as jumlah\n" +
            "from t_tf\n" +
            "group by name_term\n" +
            "order by jumlah desc";
        Connection conn =  sQLiteJDBC.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        while(rs.next()){
            MyIdf myIdf = new MyIdf();
            myIdf.term = rs.getString("name_term");
            myIdf.count = (float)rs.getInt("jumlah");
            myIdf.idf = 0.0f;
            retVal.add(myIdf);
        }
        return retVal;
    }

    public int selectTermExistInDb(String term) throws SQLException {
        int retVal = 0;
        String sql="SELECT id_term FROM t_idf where name_term = ?";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, term);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            retVal = rs.getInt("id_term");
        }
        return retVal;
    }

    public void insertIdf2t_idf(String term, double idf) throws SQLException {
        String sql="INSERT INTO t_idf(name_term, idf) VALUES(?,?)";
        
        Connection conn = sQLiteJDBC.getConnection();
        PreparedStatement pmst = conn.prepareStatement(sql);
        pmst.setString(1, term);
        pmst.setDouble(2, idf);
        pmst.execute();
    }
    
    public void updateIdf2t_idf(int id_term, double idf) throws SQLException {
        String sql="UPDATE t_idf SET idf = ? WHERE id_term = ? ";
        
        Connection conn = sQLiteJDBC.getConnection();
        PreparedStatement pmst = conn.prepareStatement(sql);
        pmst.setInt(2, id_term);
        pmst.setDouble(1, idf);
        pmst.execute();
    }

    public boolean selectContentInDB(String content) throws SQLException {
        int retVal=0;
        String sql="SELECT id_doc FROM t_doc where content_doc = ?";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, content);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            retVal = rs.getInt("id_doc");
        }
        return retVal > 0;
    }

    public String[] selectDoc() throws SQLException {
        ArrayList<String> temp = new ArrayList<>();
        String[] retVal;
        String sql="SELECT name_doc FROM t_doc ";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            String d = rs.getString("name_doc");
            d = d.substring(d.lastIndexOf("/")+1, d.length());
            temp.add(d);
        }
        retVal = temp.toArray(new String[temp.size()]);
        return retVal;
    }

    public String[] selectTerm() throws SQLException {
        ArrayList<String> temp = new ArrayList<>();
        String[] retVal;
        String sql="SELECT name_term FROM t_idf ";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            String d = rs.getString("name_term");
            temp.add(d);
        }
        retVal = temp.toArray(new String[temp.size()]);
        return retVal;
    }

    public Object[][] selectWeight(int id_doc) throws SQLException {
        ArrayList<MyWeightDoc> arrLMyWeightDoc = new ArrayList<>();
        Object[][] retVal = null;
        String sql="select a.name_term as term, a.tf, b.idf, (a.tf*b.idf) as weight\n" +
            "from t_tf a left join t_idf b on a.name_term = b.name_term\n" +
            "where a.id_doc = ?" +
            "order by weight desc";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, id_doc);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            MyWeightDoc mwd = new MyWeightDoc();
            mwd.term = rs.getString("term");
            mwd.dWeight = rs.getDouble("weight");
            arrLMyWeightDoc.add(mwd);
        }
        retVal = new Object[arrLMyWeightDoc.size()][2];
        for(int i=0;i<retVal.length;i++){
            MyWeightDoc mwd = arrLMyWeightDoc.get(i);
            retVal[i][0] = mwd.term;
            retVal[i][1] = mwd.dWeight; 
        }
        return retVal;
    
    }
    
    public int selectIdDocWhereFilenameInDB(String filename) throws SQLException{    
        int retVal=0;
        String sql="SELECT id_doc FROM t_doc where name_doc LIKE '%"+filename+"'";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        //preparedStatement.setString(1, filename);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            retVal = rs.getInt("id_doc");
        }
        return retVal;
    }
    
     public Integer[] selectAllIdDoc() throws SQLException {
        ArrayList<Integer> temp = new ArrayList<>();
        Integer[] retVal;
        String sql="SELECT id_doc FROM t_doc ";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            int d = rs.getInt("id_doc");
            temp.add(d);
        }
        retVal = temp.toArray(new Integer[temp.size()]);
        return retVal;
    }
     
    public String selectFileFromDocNameWhereId(int id_doc) throws SQLException{
        String retVal = null;
        String sql="SELECT name_doc FROM t_doc WHERE id_doc = ?";
        Connection conn =  sQLiteJDBC.getConnection();
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setInt(1, id_doc);
        ResultSet rs = preparedStatement.executeQuery();
        while(rs.next()){
            retVal = rs.getString("name_doc");
 
        }
        return retVal;
    } 
    
     
}
