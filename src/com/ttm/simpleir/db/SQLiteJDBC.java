/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author palgunadi
 */
public class SQLiteJDBC {

    private Connection c;
    private final String url = "jdbc:sqlite:db/simpleir.db";

    public SQLiteJDBC() throws Exception {
        c = null;
    }

    public void open() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection(url);
        System.out.println("Opened database successfully");
    }
    
    public Connection getConnection(){
        return c;
    }

    private void close() throws SQLException {
        c.close();
        System.out.println("Closed database successfully");
    }

    public void insert(String sql) throws SQLException {
        Statement stmt = null;
        c.setAutoCommit(false);
        stmt = c.createStatement();
        stmt.execute(sql);
        c.commit();
    }

    public ResultSet select(String sql) throws SQLException {
        ResultSet retVal = null;
        Statement stmt = c.createStatement();
        retVal = stmt.executeQuery(sql);
        return retVal;
    }

    public void selectMetaData(String sql) throws SQLException {
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery(sql);

        ResultSetMetaData rsMetaData = rs.getMetaData();

        int numberOfColumns = rsMetaData.getColumnCount();
        // System.out.println("resultSet MetaData column Count=" + numberOfColumns);

        for (int i = 1; i <= numberOfColumns; i++) {
            System.out.println("column MetaData ");
            System.out.println("column number " + i);
            // indicates the designated column's normal maximum width in
            // characters
            System.out.println(rsMetaData.getColumnDisplaySize(i));
            // gets the designated column's suggested title
            // for use in printouts and displays.
            System.out.println(rsMetaData.getColumnLabel(i));
            // get the designated column's name.
            System.out.println(rsMetaData.getColumnName(i));

            // get the designated column's SQL type.
            System.out.println(rsMetaData.getColumnType(i));

            // get the designated column's SQL type name.
            System.out.println(rsMetaData.getColumnTypeName(i));

            // get the designated column's class name.
            System.out.println(rsMetaData.getColumnClassName(i));

            // get the designated column's table name.
            System.out.println(rsMetaData.getTableName(i));

            // get the designated column's number of decimal digits.
            System.out.println(rsMetaData.getPrecision(i));

            // gets the designated column's number of
            // digits to right of the decimal point.
            System.out.println(rsMetaData.getScale(i));

            // indicates whether the designated column is
            // automatically numbered, thus read-only.
            System.out.println(rsMetaData.isAutoIncrement(i));

            // indicates whether the designated column is a cash value.
            System.out.println(rsMetaData.isCurrency(i));

            // indicates whether a write on the designated
            // column will succeed.
            System.out.println(rsMetaData.isWritable(i));

            // indicates whether a write on the designated
            // column will definitely succeed.
            System.out.println(rsMetaData.isDefinitelyWritable(i));

            // indicates the nullability of values
            // in the designated column.
            System.out.println(rsMetaData.isNullable(i));

            // Indicates whether the designated column
            // is definitely not writable.
            System.out.println(rsMetaData.isReadOnly(i));

            // Indicates whether a column's case matters
            // in the designated column.
            System.out.println(rsMetaData.isCaseSensitive(i));

            // Indicates whether a column's case matters
            // in the designated column.
            System.out.println(rsMetaData.isSearchable(i));

            // indicates whether values in the designated
            // column are signed numbers.
            System.out.println(rsMetaData.isSigned(i));

            // Gets the designated column's table's catalog name.
            System.out.println(rsMetaData.getCatalogName(i));

            // Gets the designated column's table's schema name.
            System.out.println(rsMetaData.getSchemaName(i));
        }
    }

//    public static void main(String args[]) throws Exception {
//        SQLiteJDBC sQLiteJDBC = new SQLiteJDBC();
//        String sql = "INSERT INTO t_doc (name_doc,content_doc) "
//                + "VALUES ( 'Hello1', 'How Are U1 ?' );";
//        sQLiteJDBC.insert(sql);
//        String sql = "Select * from t_doc";
//        sQLiteJDBC.select(sql);
//    }

}
