/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.doc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import jsastrawi.morphology.DefaultLemmatizer;
import jsastrawi.morphology.Lemmatizer;
/**
 *
 * @author palgunadi
 */
public class MyLemmatizer {
    private Set<String> dictionary;
    private  Lemmatizer lemmatizer;
    
    public MyLemmatizer() throws IOException{
        dictionary = new HashSet<>();
        setDictionary();
    }
    
    private void setDictionary() throws IOException {
        String[] arrContent = readFileDict();
        dictionary = new HashSet<>(Arrays.asList(arrContent));
        lemmatizer = new DefaultLemmatizer(dictionary);
    }
    
    private String[] readFileDict() throws IOException {
        String content = new String(Files.readAllBytes(Paths.get("resources/root-words.txt")));
        String[] arrContent = content.split("\n");
        return arrContent;
    }
    
    private String myLemmatize(String s){
        return lemmatizer.lemmatize(s);
    }
    
    public String[] arrMyLemmatize(String[] arrContent){
        String[] retVal = new String[arrContent.length];
        int i = 0;
        for(String s: arrContent){
            retVal[i++] = myLemmatize(s);
        }
        return retVal;
    }
    
//    public static void main(String[] args) throws IOException{
//        Set<String> dictionary = new HashSet<String>();
//        
////        InputStream in = Lemmatizer.class.getResourceAsStream("resources/root-words.txt");
////        BufferedReader br = new BufferedReader(new InputStreamReader(in));
//        
//        String content = new String(Files.readAllBytes(Paths.get("resources/root-words.txt")));
//        String[] arrContent = content.split("\n");
//        
//        dictionary = new HashSet<>(Arrays.asList(arrContent));
//        Lemmatizer lemmatizer = new DefaultLemmatizer(dictionary);
//        
//        System.out.println(lemmatizer.lemmatize("memakan"));
//        System.out.println(lemmatizer.lemmatize("meminum"));
//        System.out.println(lemmatizer.lemmatize("menidurkan"));
//    }

}
