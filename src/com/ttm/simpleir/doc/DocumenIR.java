/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.doc;

import com.ttm.basic.file.MyFile;
import com.ttm.simpleir.calc.InvDocFreq;
import com.ttm.simpleir.calc.TermFreq;
import com.ttm.simpleir.db.Model;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.time.*;

/**
 *
 * @author palgunadi
 */
public class DocumenIR {
    private final String filename;
    private String content;
    private Model model;
    private ArrayList<String> message;
    
    public DocumenIR(String filename) throws Exception{
        this.filename = filename;
        model = new Model();
        message = new ArrayList<>();
    }
    
    public static void main(String[] args) throws IOException, Exception{
        DocumenIR dIR = new DocumenIR("doc/doc1.pdf");
        dIR.process();
        MyTokenizer token = new MyTokenizer();
        String[] arrS = token.sentence2Token02(dIR.content);
        int i = 1;
        for (String string : arrS) {
            System.out.println("kata "+i+++" : "+string.toLowerCase());
        }
//        StopwordINA swi = new StopwordINA();
//        arrS = swi.cleansingStopWord(arrS);
//        System.out.println("Setelah di clean stopword");
//        i = 1;
//        for (String string : arrS) {
//            System.out.println("kata " + i++ + " : " + string.toLowerCase());
//        }
//        System.out.println("Setelah di lemmatizer ");
//        MyLemmatizer myLemmatizer = new MyLemmatizer();
//        String[] arrSL = myLemmatizer.arrMyLemmatize(arrS);
//        for (i=0; i < arrS.length; i++) {
//            System.out.println("kata " + i+1 + " : " + arrS[i]+" ==> "+arrSL[i]);
//        }
    }
    
    public void process(){
        MyFile f = new MyFile(filename);
        MyTokenizer token = new MyTokenizer();
        try{
            StopwordINA swi = new StopwordINA();
            MyLemmatizer myLemmatizer = new MyLemmatizer();
            TermFreq tf = null;
            InvDocFreq myIdf = new InvDocFreq();
            LocalDateTime currentTime = LocalDateTime.now();

            f.readPDF();
            this.content = f.getContent();
            if(!isDocExist()){

                insertDocToDb();
                message.add(currentTime+" memasukan doc "+filename+" ke dalam database");
                String[] arrS = token.Sentence2Token01(content);
                message.add(currentTime+" memisahkan content menjadi "+arrS.length+"kata ");

                arrS = swi.cleansingStopWord(arrS);
                message.add(currentTime+" membersihkan kata menjadi "+arrS.length+" kata");
                String[] arrSL = myLemmatizer.arrMyLemmatize(arrS);
                tf = new TermFreq(arrSL);
                message.add(currentTime+" meluluhkan kata");

                int idDoc = getIdDoc();
                //System.out.println("ID Doc : "+idDoc);
                tf.setIdDoc(idDoc);
                tf.setCanInsertToDb(true); // untuk proses memasukan ke dalam db maka dibuat true
                message.add(currentTime+" melakukan proses perhitungan tf");
                tf.calc();
                message.add(currentTime+" memasukan nilai tf ke database");
                message.add(currentTime+" melakukan proses perhitungan idf");
                myIdf.calc();
                message.add(currentTime+" memasukan nilai idf ke database");
            }
            else {
                message.add(currentTime+" content sudah tersedia di database");
            }
        }catch(Exception ex){
            message.add(ex.getMessage());
        }
    }
    
    private void insertDocToDb() throws Exception {
        model.insert2t_doc(filename, content);
    }
    
    private int getIdDoc() throws SQLException{
        int id_doc = 0;
        return id_doc = model.selectIdinDoc(filename);
    }
    
    public String[] getMessage(){
        return message.toArray(new String[message.size()]);
    }
    
    //TODO : perbaiki method token, titik tidak mau berfungsi

    private boolean isDocExist() throws SQLException {
        return model.selectContentInDB(content);
    }
    
}
