/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.doc;

import com.ttm.simpleir.calc.TermFreq;
import com.ttm.simpleir.calc.Weight;
import com.ttm.simpleir.db.Model;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author palgunadi
 */
public class Search {
    String query;
    MyTokenizer myTokenizer;
    StopwordINA stopwordINA;
    MyLemmatizer myLemmatizer;
    Weight weight;
    ArrayList<StWeight> arrLMyWeight;
    Model model;
    ArrayList<MySim> arrLMySim;
    
    class StWeight{
        String term;
        double weight;
    }
    
    public Search() throws IOException, Exception{
        myTokenizer = new MyTokenizer();
        stopwordINA = new StopwordINA();
        myLemmatizer = new MyLemmatizer();
        weight = new Weight();
        arrLMyWeight = new ArrayList<>();
        model = new Model();
    }
    
    public void setQuery(String query){
        this.query = query;
    }
    //TODO : please create class result, and sort it
    public void process() throws Exception {
        arrLMySim = new ArrayList<>();
        String[] arrString = myTokenizer.Sentence2Token01(query);
        arrString = stopwordINA.cleansingStopWord(arrString);
        arrString = myLemmatizer.arrMyLemmatize(arrString);
        
        TermFreq termFreq = new TermFreq(arrString);
        termFreq.setCanInsertToDb(false);
        termFreq.calc();
        Object[][] arrOTF = termFreq.getArrOTF();
        
        
        for (int i=0; i < arrOTF.length;i++) {
            String term = arrOTF[i][0].toString();
            arrOTF[i][1] = weight.getWeightQ(term,Double.parseDouble(arrOTF[i][1].toString()));
        }
        
        Integer[] id_doc = model.selectAllIdDoc();
        for(int id : id_doc){
            Double pembilang = 0.0d;
            Double penyebut = 0.0d;
            Double penyebut1 = 0.0d;
            Double sim = 0.0d;
            
            for(Object[] arrOTF1 : arrOTF ){
                String term = arrOTF1[0].toString();
                Double wTermInDoc = weight.getWeightD(id,term);
                Double wTermInQ = (double)arrOTF1[1];
                if(wTermInDoc < Double.MAX_VALUE && wTermInQ < Double.MAX_VALUE){
                    pembilang = pembilang + (wTermInDoc*wTermInQ);
                    penyebut = penyebut + Math.pow(wTermInDoc, 2.0d);
                    penyebut1 = penyebut1 + Math.pow(wTermInQ, 2.0d);
                }
            }
            
            penyebut = Math.sqrt(penyebut*penyebut1);
            sim = pembilang/penyebut;
            String name_doc = model.selectFileFromDocNameWhereId(id);
            MySim mySim = new MySim(id, name_doc, sim);
            if(sim < Double.MAX_VALUE){
                arrLMySim.add(mySim);
            }
        }
        Collections.sort(arrLMySim, (MySim mysim1, MySim mysim2) -> Double.compare(mysim2.getSim(), mysim1.getSim()));
    }
    
    public ArrayList<MySim> getArrayLMySim(){
        return arrLMySim;
    }
    
}
