/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.doc;

/**
 *
 * @author palgunadi
 */
import java.util.Comparator;

public class MySim {
    private int id_doc;
    private String filename;
    private double sim;
    
    public MySim(int id_doc, String filename, double sim){
        this.id_doc = id_doc;
        this.filename = filename;
        this.sim = sim;
    }
    
    public void setSim(double sim){
        this.sim = sim;
    }
    
    public double getSim(){
        return this.sim;
    }
    
    public String getNameDoc(){
        return this.filename;
    }
    
}
