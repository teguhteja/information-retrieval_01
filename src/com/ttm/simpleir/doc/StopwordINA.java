/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.doc;

import com.ttm.basic.file.MyFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author palgunadi
 */
public class StopwordINA {
    private String[] arrStopword;
    private List<String> lSStopWord;
    private final String fileStopword = "stopword/id.stopwords.02.01.2016.txt";
    
    public StopwordINA() throws IOException{
        obtainStopWordFromFile();
    
    }
    
    private void obtainStopWordFromFile() throws IOException{
        MyFile f = new MyFile(fileStopword);
        f.read();
        arrStopword = f.getContent().split("\n");
        lSStopWord = Arrays.asList(arrStopword);
    }
    
    public String cleaningStopWord(String content){
        String retVal="";
        retVal = lSStopWord.stream()
            .map(toRem-> (Function<String,String>)s->s.replaceAll(toRem, ""))
            .reduce(Function.identity(), Function::andThen)
            .apply(content);
        return retVal;
    }
    
    public String[] cleansingStopWord(String[] kata){
        String[] retVal = null;
        ArrayList<String> temp = new ArrayList<>();
        for(int i = 0; i < kata.length;i++){
            Boolean isNotStopWord = true;
            for(int j = 0; j < arrStopword.length; j++){
                if(kata[i].equalsIgnoreCase(arrStopword[j])){
                    isNotStopWord = false;
                    j=arrStopword.length;
                }
            }
            if(isNotStopWord){
                temp.add(kata[i]);
            }
        }
        retVal = new String[temp.size()];
        retVal = temp.toArray(retVal);
        return retVal;
    }
}
