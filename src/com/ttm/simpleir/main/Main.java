/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.main;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 *
 * @author palgunadi
 */
public class Main {

    public static void main(String[] args) throws IOException {
       //test();
       //testSkin();
       testDouble();
    }
    
    private static void test() {
        for (int i = 0; i < 10; i++) {
            System.out.print("#");
            for (int j = 0; j < 10 - 2; j++) {
                if (i == 0 || i == 9 || i - 1 == j || i + j == 8) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("#");
        }
    }
    
    private static void testSkin(){
         for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            System.out.println(info.getName());
        }
    }

    private static void testDouble() {
        boolean b = -0.45d < Double.MAX_VALUE; 
        System.out.println(b);
    }
}
