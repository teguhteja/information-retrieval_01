/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.calc;

import com.ttm.simpleir.db.Model;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author palgunadi
 */
public class TermFreq {
    private String[] arrTerm;
    private ArrayList<String> arrLTerm;
    private ArrayList<MyTermFreq> arrLTerm2;
    private Model model;
    private int id_doc;
    private boolean canInsertToDb;
    private Object[][] arrOTF;
    
    public TermFreq(String[] arrTerm) throws Exception{
       this.arrTerm =  arrTerm;
       model = new Model();
       canInsertToDb = false;
    }
    
    class MyTermFreq{
        String term;
        float freq;
    }
    
    public void setIdDoc(int id_doc){
        this.id_doc = id_doc;
    }
    
    public void setCanInsertToDb(boolean value){
        canInsertToDb = value;
    }
    
    public void calc() throws SQLException{
        arrLTerm = new ArrayList<>(Arrays.asList(arrTerm));
        arrLTerm2 = new ArrayList<>();
        
        float iArrTerm = arrTerm.length;
        for(int i = 0; i < arrLTerm.size();i++){
            String term1 = arrLTerm.remove(i);
            float count = 1;
            for(int j=i; j<arrLTerm.size();j++){
                String term2 = arrLTerm.get(j);
                if(term1.equalsIgnoreCase(term2)){
                    term2 = arrLTerm.remove(j);
                    count++;
                    j--;
                }
            }
            MyTermFreq mtf = new MyTermFreq();
            mtf.term = term1;
            mtf.freq = count/iArrTerm;
            arrLTerm2.add(mtf);
            i--;
        }
        
            // System.out.println("Jumlah data yang dimasukan = "+arrLTerm2.size());
            arrOTF = new Object[arrLTerm2.size()][2];
            int i = 0;
            for(MyTermFreq mtf : arrLTerm2){
                //System.out.println(mtf.term+" = "+String.format("%.4f", mtf.freq)+" = "+mtf.freq);
                arrOTF[i][0] = mtf.term;
                arrOTF[i][1] = mtf.freq;
                
                if(canInsertToDb){
                    insertTF2Db(mtf.term, mtf.freq);
                }
                i++;
            }
    }
    
    private void insertTF2Db(String term, float freq) throws SQLException {
        model.insert2t_term(id_doc,term,freq);
    }
    
    public Object[][] getArrOTF(){
        return arrOTF;
    }
}
