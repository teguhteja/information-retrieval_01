/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.calc;

import com.ttm.simpleir.db.Model;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author palgunadi
 */
public class InvDocFreq {
    Model model = null;
    
    
    public InvDocFreq() throws Exception{
        model = new Model();
    }
    
    public static void main(String[] args) throws Exception{
        InvDocFreq invDocFreq = new InvDocFreq();
        invDocFreq.calc();
    }
    // Check perhitungan IDF
    public void calc() throws SQLException{
       float N = (float)getTotalDoc();
       ArrayList<MyIdf> arrLMyIdf = new ArrayList<>();
       arrLMyIdf = getDistTerm();
       calc_idf(arrLMyIdf,N);
    }
    
    private int getTotalDoc() throws SQLException {
        int retVal = 0;
        retVal = model.selectCountDoc();
        return retVal;
    }

    private ArrayList<MyIdf> getDistTerm() throws SQLException {
        return model.selectDistTerm();
    }
    
    private void calc_idf(ArrayList<MyIdf> arrLMyIdf, float N) throws SQLException {
        for(MyIdf myIdf : arrLMyIdf){
            float n = myIdf.count;
            double idf = Math.log10((double)n/(double)N);
            int id_term =0;
            id_term = isTermExistInDb(myIdf.term);
            if(id_term >0){
                updateIdf2Db(id_term,idf);
                //System.out.println("update "+myIdf.term);
            } else{
                insertIdf2Db(myIdf.term,idf);
                //System.out.println("insert "+myIdf.term);
            }
        }
    }

    private int isTermExistInDb(String term) throws SQLException{
        return model.selectTermExistInDb(term);
    }

    private void updateIdf2Db(int id_term, double idf) throws SQLException {
        model.updateIdf2t_idf(id_term,idf);
    }

    private void insertIdf2Db(String term, double idf)throws SQLException {
        model.insertIdf2t_idf(term,idf);
    }
}
