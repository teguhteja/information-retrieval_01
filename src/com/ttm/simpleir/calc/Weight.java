/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.simpleir.calc;

import com.ttm.simpleir.db.Model;
import java.sql.SQLException;

/**
 *
 * @author palgunadi
 */
public class Weight {
    private Model model;
    private String[] docs;
    
    public Weight() throws Exception{
        model = new Model();
    }
    
    public String[] getDoc() throws SQLException{
        return model.selectDoc();
    }

    public String[] getTerm() throws SQLException {
        return model.selectTerm();
    }
    
    public Object[][] getWeightDoc(int id_doc) throws SQLException{
        return model.selectWeight(id_doc);
    }

    public int getIdDoc(String name_doc) throws SQLException {
        return model.selectIdDocWhereFilenameInDB(name_doc);
    }

    public Object[][] getWeightTermInAllDoc(String term) throws SQLException {
        return model.selectWeightTerm(term);
    }

    public double getWeightQ(String term, Double tf) throws SQLException {
        double idf = model.selectIdf(term);
        double weight = idf > 0.0d ? 1.0d : tf*idf;
        return weight;
    }

    public Double getWeightD(int id, String term) throws SQLException {
        return model.selectWeightBasedIdTerm(id,term);
    }
}
