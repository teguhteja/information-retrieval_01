/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ttm.basic.file;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author palgunadi
 */
public class MyFile {
    String filename;
    String content;
    
    public MyFile(String name){
        this.filename = name;
    }
    
    public void read() throws IOException{
        this.content = new String(Files.readAllBytes(Paths.get(filename)));
    }
    
    public void readPDF() throws IOException{
         try (PDDocument document = PDDocument.load(new java.io.File(filename))) {

            document.getClass();

            if (!document.isEncrypted()) {
			
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                
                PDFTextStripper tStripper = new PDFTextStripper();
                this.content = tStripper.getText(document);
                
                //String pdfFileInText = tStripper.getText(document);
               
                // String lines[] = pdfFileInText.split("\\r?\\n");
                // for (String line : lines) {
                //     System.out.println(line);
                // }

            }
         }
    }
    
    public String getContent(){
        return content;
    }
}
