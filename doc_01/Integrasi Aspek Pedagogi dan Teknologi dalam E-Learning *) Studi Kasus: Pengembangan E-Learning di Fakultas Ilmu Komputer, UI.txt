Integrasi Aspek Pedagogi dan Teknologi dalam E-Learning *)
Studi Kasus: Pengembangan E-Learning di Fakultas Ilmu Komputer, UI
Oleh: Zainal A. Hasibuan
Fakultas Ilmu Komputer, Universitas Indonesia (UI)
zhasibua@cs.ui.ac.id


Abstrak

Paper ini akan membahas integrasi dua aspek yang saling berpengaruh dalam e-learning, yaitu pedagogi dan teknologi. Aspek pedagogi sangat berpengaruh terhadap pengembangan materi (content development) dan pembelajaran (learning) dengan memperhatikan teknik dan behavior interaksi antara dosen (lecturer) dan mahasiswa (students). Di sisi lain aspek teknologi berpengaruh terhadap pengembangan materi yang dinamis dan rich multimedia, serta menyediakan fitur-fitur (features) komponen system e-learning. Mengintegrasikan kedua aspek tersebut dapat meningkatkan kualitas pengembangan materi yang didukung oleh system e-learning agar proses belajar-mengajar menjadi optimal. Subjective Analysis atas pengintegrasian kedua aspek tersebut dalam system e-learning yang digunakan – SCELE, menunjukkan bahwa lingkungan pembelajaran online dapat menghadirkan suasana sebagaimana pembelajaran konvensional.  
